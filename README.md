## LuaSpace-vscode
  VSCode Workspace and DevContainer for Lua Development.   

### Install:   
  `git clone` or download and extract the `.devcontainer` folder and its contents to a _fresh workspace_   
  **when the container has started for the first time you need to follow the prompts in the in the (zsh) terminal.**

### Requirements:
- [docker](https://docs.docker.com/)
- vscode with the following plugins
  - ms-vscode-remote.remote-containers    
    VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers
  - aaron-bond.better-comments   
    VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=aaron-bond.better-comments
  - actboy168.extension-path   
    Description: New Command: extensionPath   
    VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=actboy168.extension-path
  - sumneko.lua   
    Description: Lua Language Server coded by Lua   
    VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=sumneko.lua

##### Dev Container Features:

- [alpine:3.11](https://alpinelinux.org/) Image with C and C++ compilers (GCC) (154MB)
- AutoInstalls Linux Brew on first run.
  - NOTE: You should Immediately Update Brew and Reinstall gcc from source on your First run
    - `brew update && brew install gcc`
    - reinstalling gcc may take some time but some programs will likely fail to compile/install via
      Linux brew if this step isn't done
-  installed `unzip`|`nano`|`make`|`git`|`curl`|`wget`|`tmux`|`musl-dev`|`glibc`|`gcc`|`python2`|`ruby`
- [lua5.1.5](http://www.lua.org/manual/5.1/manual.html) with [luarocks3.3.0](https://luarocks.org/)
  - [busted](https://olivinelabs.com/busted/) - Lua Unit Testing
  - [moonscript](https://moonscript.org/) - Dynamic scripting language that compiles into [Lua](https://www.lua.org/).
  - [moonpick](https://github.com/nilnor/moonpick) - Moonscript Linter
- Latest [Golang](https://golang.org/) installed from alpine packages
  - [Glide](https://github.com/Masterminds/glide) - Go Package Management
  - [GoTTY](https://github.com/yudai/gotty) - Web Shell Access
    - Installed globaly though not active by default, check out the linked docs for usage
- Latest [Algernon](https://algernon.roboticoverlords.org/) Compiled from sources

###### VSCode Workspace Features:

- Preinstalled Extensions
  - Name: Error Lens   
    Description: Improve highlighting of errors, warnings and other language diagnostics.   
    VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=usernamehw.errorlens
  - sumneko.lua   
    Description: Lua Language Server coded by Lua   
    VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=sumneko.lua
  - Name: vcterminal      
    Description: Open Integrated Terminal for Visual Studio toolchain/Ubuntu Bash   
    VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=espresso3389.vcterminal
  - Name: MoonScript   
    Description: Working MoonScript support for vscode.   
    VS Marketplace Link: https://marketplace.visualstudio.com/items?itemName=vgalaktionov.moonscript

